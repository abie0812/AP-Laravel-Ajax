<?php

namespace App\Http\Controllers;

use App\Sex;
use App\Student;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function index() 
    {
        return view('ajax.index');
    }

    public function readData() 
    {
        $students = Student::join('sexes', 'sexes.id', '=', 'students.sex_id')
                    ->selectRaw('sexes.sex,
                               students.first_name,
                               students.last_name,
                               CONCAT(students.first_name, " ", students.last_name) AS full_name, 
                               students.id')
                    ->get();

        /** Gunakan ini ketika ingin me-return data dalam JSON */
        // return response($students);

        return view('ajax.studentList', compact('students'));
    }

}
