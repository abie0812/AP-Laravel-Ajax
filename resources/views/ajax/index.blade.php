@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dashboard
                    <button class="btn btn-info btn-xs pull-right" id="read-data">Load Data By Ajax</button>
                </div>

                <div class="panel-body">
                    <table class="table table-bordered table-striped table-condensed">
                        <thead>
                            <th>Id</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Full Name</th>
                            <th>Action</th>
                        </thead>
                        <tbody id="student-info">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#read-data').on('click', function() {
            $.get("{{ URL::to('student/read-data') }}", function(data) {

                $('#student-info').empty().html(data);

                /* supaya data diload cukup 1x saja */
                // $('#student-info').empty();

                /* looping sebanyak data yang diperoleh dari JSON */
                // $.each(data, function(i, value) {
                //     var tr =  $("<tr/>");
                //         tr.append($("<td/>", {
                //             text : value.id
                //         })).append($("<td/>", {
                //             text : value.first_name
                //         })).append($("<td/>", {
                //             text : value.last_name
                //         })).append($("<td/>", {
                //             text : value.full_name
                //         })).append($("<td/>", {
                //             html : "<a href='#'>View</a> <a href='#'>Edit</a> <a href='#'>Delete</a>"
                //         }))
                //     $('#student-info').append(tr);

                // })
            })
        });
    </script>
@endsection
