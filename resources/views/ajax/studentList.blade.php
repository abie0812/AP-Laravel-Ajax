@foreach($students as $student)
    <tr>
        <td>{{ $student->id }}</td>
        <td>{{ $student->first_name }}</td>
        <td>{{ $student->last_name }}</td>
        <td>{{ $student->full_name }}</td>
        <td>
            <a href="#" class="btn btn-info btn-xs" data="{{ $student->id }}">View</a>
            <a href="#" class="btn btn-success btn-xs" data="{{ $student->id }}">Edit</a>
            <a href="#" class="btn btn-danger btn-xs" data="{{ $student->id }}">Delete</a>
        </td>
    </tr>
@endforeach