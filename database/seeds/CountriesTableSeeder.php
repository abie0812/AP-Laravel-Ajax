<?php

use App\Country;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();

        Country::insert([
            ['country' => 'Indonesia', 'created_at' => $now, 'updated_at' => $now],
            ['country' => 'Australia', 'created_at' => $now, 'updated_at' => $now],
            ['country' => 'Singapore', 'created_at' => $now, 'updated_at' => $now],
            ['country' => 'Malaysia', 'created_at' => $now, 'updated_at' => $now],
            ['country' => 'Croatia', 'created_at' => $now, 'updated_at' => $now],
            ['country' => 'New Zealand', 'created_at' => $now, 'updated_at' => $now],
            ['country' => 'Hawai', 'created_at' => $now, 'updated_at' => $now],
            ['country' => 'USA', 'created_at' => $now, 'updated_at' => $now],
        ]);
    }
}
