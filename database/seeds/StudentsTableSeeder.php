<?php

use App\Student;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();

        Student::insert([
            ['first_name' => 'Stefan', 'last_name' => 'William', 'sex_id' => 1],
            ['first_name' => 'Kevin', 'last_name' => 'Julio', 'sex_id' => 1],
            ['first_name' => 'Adipati', 'last_name' => 'Dolken', 'sex_id' => 1],
            ['first_name' => 'Iko', 'last_name' => 'Uwais', 'sex_id' => 1],
            ['first_name' => 'Oka', 'last_name' => 'Antara', 'sex_id' => 1],
            ['first_name' => 'Eva', 'last_name' => 'Celia', 'sex_id' => 2],
            ['first_name' => 'Ranty', 'last_name' => 'Maria', 'sex_id' => 2],
            ['first_name' => 'Jessica', 'last_name' => 'Mila', 'sex_id' => 2],
            ['first_name' => 'Pevita', 'last_name' => 'Pearce', 'sex_id' => 2],
            ['first_name' => 'Aurelia', 'last_name' => 'Mouremans', 'sex_id' => 2],
        ]);
    }
}
