<?php

use App\Sex;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class SexesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();

        Sex::insert([
            ['sex' => 'Male', 'created_at' => $now, 'updated_at' => $now],
            ['sex' => 'Female', 'created_at' => $now, 'updated_at' => $now],
        ]);
    }
}
