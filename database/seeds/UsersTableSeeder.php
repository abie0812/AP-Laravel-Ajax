<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();

        User::insert([
            ['name' => 'admin', 'email' => 'admin@gmail.com', 'password' => bcrypt('password'), 'active' => 1,  'created_at' => $now, 'updated_at' => $now],
        ]);
    }
}
