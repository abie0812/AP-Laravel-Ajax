## Alur dari READ data
1. Apabila user meng-klik button 'Load Data By Ajax', maka akan mentrigger
   sebuah function.
2. function ini akan melakukan proses link ke url '/student/read-data' 
   dimana dipegang oleh AjaxController method index. Proses ini tidak diketahui user, sehingga
   user hanya taunya datanya ditampilkan.
3. Dari AjaxController method index akan me-return nilai ke studentList.blade.php.
4. Karena function yang me-link url '/student/read-data' kita tulis untuk melakukan callback yang meminta 'data',
   maka data inilah yaitu studentList.blade.php yang sudah diberi nilai sebagai nilai dari yang di return.
5. Terakhir, select tbody dengan id '"student-info' untuk dimasukkan nilai yang tadi didapat.

## Alur dari EDIT